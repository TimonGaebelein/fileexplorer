const fs = require('fs-extra')
const path = require('path');
const helper = require('./helper')
const userID = require('userid')

let filesystem = FileSystem.prototype

function FileSystem(directory) {
    filesystem.baseDirectory = directory
    console.log("Filesystem ready (Base Path: " + __dirname + "/" + this.baseDirectory + ")")
}


filesystem.listDirectory = async function(directory) {
    try {
        items = await fs.readdir(path.join(this.baseDirectory, directory));
    } catch (err) {
        throw err
    }
    return items;
}

filesystem.getMetaInfo = async function(directory, file) {
    let content = {}
    content.absPath = path.join(__dirname, this.baseDirectory, directory, file)
    try {
        stat = await fs.stat(content.absPath)
        content.name = file
        content.directory = stat.isDirectory()
        content.size = helper.getSizeString(stat.size)
        content.created = stat.ctime.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        content.modified = stat.mtime.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        content.owner = userID.username(stat.uid)
        if (!content.directory) {
            content.type = helper.getFileType(content.absPath)
        } else {
            content.type = null
        }
    } catch (err) {
        throw err
    }
    return content
}

filesystem.getDirectory = async function(directory) {
    directory = path.normalize(directory).replace("..", "");
    console.log(directory)
    let contents = {
        directory: directory,
        timestamp: + new Date(),
        contents: []
    }
    try {
        items = await this.listDirectory(directory)
        for(item of items) {
            meta = await this.getMetaInfo(directory, item)
            contents.contents.push(meta)
        }
    } catch (err) {
        throw err
    }
    return contents
}

filesystem.checkFileExists = async function(filePath) {
    filePath = path.join(__dirname, this.baseDirectory, path.normalize(filePath.replace("..", "")))
    try {
        if (!await fs.exists(filePath)) {
            throw new Error("ENOENT: File does not exist!")
        }
    } catch (err) {
        throw err
    }
    return filePath
}

module.exports = FileSystem;