import { AppComponent } from './../app.component';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-grid-view',
  templateUrl: './grid-view.component.html',
  styleUrls: ['./grid-view.component.scss']
})
export class GridViewComponent implements OnInit {
  @Input() path: String[];
  @Input() content: String[];

  constructor(private _app: AppComponent) { }

  ngOnInit() {
  }

}
