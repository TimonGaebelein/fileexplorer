import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  apiUrl = 'http://localhost:8082/api/';

  constructor() {}

  getBaseUrl = () => {
    return new Promise((resolve, reject) => {
      $.ajax({
        'async': true,
        'crossDomain': true,
        'url': this.apiUrl + 'base_directory',
        'method': 'GET',
        'headers': {
          'Cache-Control': 'no-cache'
        }
      })
      .done((response) => {
        resolve(response.base);
      })
      .fail((err) => {
        reject(err);
      });
    });
  }

  loadDirectoryContents = (dir) => {
    if (dir === '.') {
      return new Promise((resolve, reject) => {
        $.ajax({
          'async': true,
          'crossDomain': true,
          'url': this.apiUrl + 'fs',
          'method': 'GET',
          'headers': {
            'Cache-Control': 'no-cache'
          }
        })
        .done((response) => {
          resolve(response);
        })
        .fail((err) => {
          reject(err);
        });
      });
    } else {
      return new Promise((resolve, reject) => {
        $.ajax({
          'async': true,
          'crossDomain': true,
          'url': this.apiUrl + 'fs' + dir,
          'method': 'GET',
          'headers': {
            'Cache-Control': 'no-cache'
          }
        })
        .done((response) => {
          resolve(response);
        })
        .fail((err) => {
          reject(err);
        });
      });
    }
  }
}
