import { AppComponent } from './../app.component';
import { Component, OnInit, Input } from '@angular/core';

import { faFolder } from '@fortawesome/free-solid-svg-icons';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { faFileImage } from '@fortawesome/free-solid-svg-icons';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {
  @Input() path: String[];
  @Input() content: String[];

  folderIcon = faFolder;
  fileIcon = faFile;
  fileImageIcon = faFileImage;
  fileTextIcon = faFileAlt;

  constructor(private _app: AppComponent) { }

  ngOnInit() {
    console.log(this.content);
  }

}
