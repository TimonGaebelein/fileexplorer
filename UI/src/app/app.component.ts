import { AppService } from './app.service';
import { Component } from '@angular/core';

import * as _ from 'lodash';

import { faList } from '@fortawesome/free-solid-svg-icons';
import { faThLarge } from '@fortawesome/free-solid-svg-icons';
import * as fontawesome from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  basePath;
  path = [];
  content = [];
  timestamp;
  gridView = true;

  listIcon = faList;
  gridIcon = faThLarge;

  constructor(private _service: AppService) {
    this._service.getBaseUrl()
    .then((baseDirectory) => {
      this.basePath = baseDirectory;
    })
    .catch((err) => {
      console.log('ERROR: ' + err);
    });
    this.changeDir('.');
  }

  changeDirExtend = (dir) => {
    this.changeDir('/' + this.path.slice(0, this.path.indexOf(dir) + 1).join('/'));
  }

  changeOrOpen = (dir, isDirectory) => {
    if (isDirectory) { this.changeDir('/' + this.path.join('/') + '/' + dir); }
  }

  changeDir = (dir) => {
    this._service.loadDirectoryContents(dir)
    .then((contents) => {
      if (contents['directory'] === '.') { this.path = []; } else { this.path = contents['directory'].split('/'); }
      this.timestamp = contents['timestamp'];
      this.content = contents['contents'];

    })
    .catch((err) => {
      console.log('ERROR: ' + err);
    });
  }

  getFileIcon = (absPath) => {
    // console.log(this.getFileIcon(absPath))
    switch (_.last(absPath.split('.'))) {
      case 'png':
        return fontawesome.faFileImage;
      case 'txt':
        return fontawesome.faFileAlt;
      default:
        return fontawesome.faFile;
    }
  }
}
