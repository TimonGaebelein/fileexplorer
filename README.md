# NodeFile

## For Developers

### API

All API endpoints are prefixed with _/api/_

#### Filesystem

##### GET /fs/:path

Retrieves the contents of the requested path with some meta information. If the path is left empty the contents of the directory, configured in the configuration file will be used.

### Errors

- 0 - Unknown
- 1 - File / Directory not found