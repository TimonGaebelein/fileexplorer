const port = process.env.PORT || 8082;
let config = require('./config')
let FileSystem = require('./filesystem')
let Server = require('./server')

let fs = new FileSystem(config.directory)
let server = new Server(port, fs)

// async function main() {
//     dir = await fs.getDirectory(config.directory)
//     console.log(dir)
// }

// main()