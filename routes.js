const express = require('express')
const path = require('path');

function Routes(fs) {
    this.fs = fs
    this.router = express.Router()

    this.router.get("/", (req, res) => {
        return res.send("Welcome to the API! Documentation can be found here: https://bitbucket.org/TimonGaebelein/fileexplorer#readme")
    })

    this.router.route('/base_directory')
    .get((req, res) => {return res.json({base: this.fs.baseDirectory})})

    this.router.route('/fs/*')
    .get((req, res) => {this.getDirectory(req, res)})

    this.router.route('/fs')
    .get((req, res) => {this.getDirectory(req, res)})

    this.router.route('/file')
    .get((req, res) => {return res.json({"error": "please specify file!"})})

    this.router.route('/file/*')
    .get((req, res) => {this.getFile(req, res)})

    this.getDirectory = async (req, res) => {
        let dir;
        if (req.params[0] == undefined) {
            dir = "."
        } else {
            dir = path.join.apply(null, req.params[0].split("/"))
        }
        try {
            contents = await fs.getDirectory(dir)
            return res.json(contents)
        } catch (err) {
            if (err.code === "ENOENT") {
                return res.json({error: 1, message: "File or Directory does not exist."})
            }
            if (err.code === "EACCES") {
                return res.json({error: 2, message: "Access denied."})
            }
            return res.json({error: 0, message: err.toString()})
        }
    }

    this.getFile = async (req, res) => {
        filePath = path.join.apply(null, req.params[0].split("/"))
        try {
            filePath = await fs.checkFileExists(filePath)
            return res.sendFile(filePath)
        } catch (err) {
            if (err.code === "ENOENT") {
                return res.json({error: 1, message: "File or Directory does not exist."})
            }
            if (err.code === "EACCES") {
                return res.json({error: 2, message: "Access denied."})
            }
            return res.json({error: 0, message: err.toString()})
        }
    }
}


module.exports = Routes