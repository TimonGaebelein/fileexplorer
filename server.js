const express = require("express")
let cors = require("cors")
const bodyParser = require("body-parser")
const Routes = require('./routes')

let server = Server.prototype

function Server(port, fs) {
    this.app = express()
    this.routes = new Routes(fs)
    this.app.use(cors())
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(express.static("UI/dist/UI"))
    this.app.use('/api', this.routes.router)
    this.app.listen(port, () => {
        console.log("Server listening on port " + port)
    })
}

module.exports = Server