const readChunk = require('read-chunk')
const fileType = require('file-type')
const isSVG = require('is-svg')

let helper = {};

helper.getSizeString = (size) => {
    // Convert integer size to string, (bytes, kilobytes, megabytes, gigabytes, terabytes)
    if (size <= 1000) return size + " B"
    if (size <= 1000000) return (size / 1000).toFixed(1) + " KB"
    if (size <= 1000000000) return (size / 1000000).toFixed(1) + " MB"
    if (size <= 1000000000000) return (size / 1000000000).toFixed(1) + " GB"
    else {return (size / 1000000000000).toFixed(1) + " TB"}
}

helper.getFileType = (path) => {
    // Get the file type, based on the Magic Number
    // To be added: SVG, XLSX, PPTX, DOCX
    let buffer = readChunk.sync(path, 0, 4100)
    type = fileType(buffer)
    if (type === null) {
        if (path.split(".")[path.split(".").length - 1] === "txt") return "text/plain"
    }
    if (type !== null) {
        return type.mime
    } else {
        return null
    }
}

module.exports = helper;